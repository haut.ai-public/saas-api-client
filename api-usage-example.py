#!/usr/bin/env python3

import base64
import datetime as dt
import json
import time
from io import BytesIO
from pprint import pprint

import requests

with open("credentials.json") as f:
    credentials = json.load(f)

# TODO: Read from file.
USERNAME = credentials["username"]
PASSWORD = credentials["password"]

DOMAIN = "saas.haut.ai"
API_VERSION = "v1"
API_ENDPOINT = f"https://{DOMAIN}/api/{API_VERSION}"
LOGIN_ENDPOINT = f"https://{DOMAIN}/api/login/"
IMAGE_FILE = "image.jpg"  # An image to be upload.


def report(header, resp):
    print(header)
    print(f"Status: {resp.status_code}")
    if resp.headers.get("Content-Type") == "application/json":
        pprint(resp.json())
    else:
        print(r.text)
    print()


# Login
r = requests.post(LOGIN_ENDPOINT, {"username": USERNAME, "password": PASSWORD})
report("Login responce:", r)

# Remember credentials
access_token = r.json()["token_access"]
refresh_token = r.json()["token_refresh"]  # Refresh token is not used at the moment.

headers = {"Authorization": f"Bearer {access_token}"}
company_id = r.json()["company_id"]

# List available algorithms
r = requests.get(f"{API_ENDPOINT}/algorithms/", headers=headers)
algorithms = [
    {k: v for k, v in algo.items() if k in {"name", "versions"}}
    for algo in r.json()["results"]
]
report("Algorithms response:", r)
print("Current algorithms:")
pprint(algorithms)
print()

# Get info on one algorithm.
# r = requests.get(f'{API_ENDPOINT}/algorithms/8', headers=headers)

# Get company info.
# TODO: Does not work with COMPANY_NAME (slag)
r = requests.get(f"{API_ENDPOINT}/companies/{company_id}/", headers=headers)
report("Companies response:", r)

# Get all datasets.
# TODO: Should have user_id in path.
r = requests.get(f"{API_ENDPOINT}/companies/{company_id}/datasets/", headers=headers)
report("Datasets response:", r)

# Remember our dataset_id.
dataset_id = r.json()["results"][0]["id"]

# Get dataset info.
r = requests.get(
    f"{API_ENDPOINT}/companies/{company_id}/datasets/{dataset_id}/", headers=headers
)
report("Dataset response:", r)

# Remeber out subject_id
subject_name = "subject1"

# Check if subject exists
r = requests.get(
    f"{API_ENDPOINT}/companies/{company_id}/datasets/{dataset_id}/subjects/{subject_name}/",
    headers=headers,
)
if r.status_code == 200:
    print("Deleteing existing subject")
    # Remove subject if exists to start from scratch.
    r = requests.delete(
        f"{API_ENDPOINT}/companies/{company_id}/datasets/{dataset_id}/subjects/subject1/",
        headers=headers,
    )
    report("Delete subject response:", r)

# Create a new subject to post photos for.
# TODO: Returns 500 if a subject already esists, should return 'exists' 409.
r = requests.post(
    f"{API_ENDPOINT}/companies/{company_id}/datasets/{dataset_id}/subjects/",
    json={"name": subject_name},
    headers=headers,
)
report("Post subject response:", r)

# TODO: Slug does not work :(
# subject_id = r.json()['slug']  # It is also possible to use id: subject_id = r.json()['id']
subject_id = r.json()["id"]

with open(IMAGE_FILE, "rb") as f:
    bio = BytesIO()
    base64.encode(f, bio)

r = requests.post(
    f"{API_ENDPOINT}/companies/{company_id}/datasets/{dataset_id}/subjects/{subject_id}/photos/stream/",
    headers=headers,
    json={"b64data": bio.getvalue().decode("utf8")},
)
report("Post an image response:", r)

photo_id = r.json()["id"]

# TODO: Think on implementig a synchronous method too.
retries_left = 6
sleep_seconds = 15
algos = {x["name"] for x in algorithms}
while retries_left:
    r = requests.get(
        f"{API_ENDPOINT}/companies/{company_id}/datasets/{dataset_id}/subjects/{subject_id}/photos/{photo_id}/measures/",
        headers=headers,
    )
    ready_algos = {x["algo_name"] for x in r.json()["measures"]}
    if r.status_code != 200:
        report("Measures returned:", r)
    elif ready_algos == algos:
        print("All measures are ready!")
        break
    print("Measures are not yet ready, retrying...")
    retries_left -= 1
    time.sleep(sleep_seconds)
report("Measures response:", r)

# Now we can get some insights on the subject.
start_date = dt.datetime.today()
end_date = start_date + dt.timedelta(days=1)
r = requests.get(
    f"{API_ENDPOINT}/companies/{company_id}/datasets/{dataset_id}/subjects/{subject_id}/get_measures_timeline/",
    # The span is closed, the right border is included.
    params={
        "start_date": start_date.strftime("%Y.%m.%d %H:%M:%S"),
        "end_date": end_date.strftime("%Y.%m.%d %H:%M:%S"),
    },
    headers=headers,
)
report(f"get_measures_timeline responce:", r)

for method in ["last_measures", "last_photo"]:
    r = requests.get(
        f"{API_ENDPOINT}/companies/{company_id}/datasets/{dataset_id}/subjects/{subject_id}/{method}/",
        headers=headers,
    )
    report(f"{method.capitalize()} responce:", r)

# And get some stats on our company.
for method in [
    "datasets_counter",
    "my_datasets",
    "photos_counter",
    "photos_timeline",
    "subjects_counter",
    "users_counter",
]:
    r = requests.get(
        f"{API_ENDPOINT}/companies/{company_id}/{method}/", headers=headers,
    )
    report(f"{method.capitalize()} responce:", r)

# Cleanup subject.
r = requests.delete(
    f"{API_ENDPOINT}/companies/{company_id}/datasets/{dataset_id}/subjects/subject1/",
    headers=headers,
)
report("Delete subject response:", r)
