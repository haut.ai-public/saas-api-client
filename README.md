# HautAI SaaS API consuming example.
This repository contains the code with actual example of how to use saas.haut.ai API.

## Usage

1. Clone this repository to your machine
    ```
    git clone https://gitlab.com/haut.ai-public/saas-api-client.git
    cd saas-api-client
    ```
1. Install dependencies
    ```
    python3 -m venv venv
    source venv/bin/activate
    python3 -m pip install -r requirements.txt
    ```
1. Add your credentials to credentials.json
    ```
    read -p "Enter username: " HAUTUSER   && \
    read -p "Enter password: " HAUTPASSWD && \
    cat <<END > credentials.json
    {
        "username": "$HAUTUSER",
        "password": "$HAUTPASSWD"
    }
    END
    ```
1. Run the script
    ```
    python3 api-usage-example.py
    ```

Also if you use VSCode with [remote-container](https://github.com/Microsoft/vscode-remote-release.git) extension, you can just open this folder in it and run api-usage-example.py script.

## Documentation
The API docs can be found [here](https://saas.haut.ai/api/swagger).

## Issues and help
Please feel free to submit any issues regarding this script or the API behaviour to this repo [issues board](https://gitlab.com/haut.ai-public/saas-api-client/issues).

Fell free to contact Konstantin (kk@haut.ai), Alexey (a.atanov@haut.ai) or dev@haut.ai on technical questions.

Please contact Anastasia (nastya@haut.ai) on topics regarding business and partnership.
